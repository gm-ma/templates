const ejs = require('ejs'),
      fs = require('fs'),
      templatesFolder = './templates/';

fs.readdir(templatesFolder, (err, files) => {
  if(err) throw Error('Error reading templates');
  Promise.all(
    files.map(template => 
      ejs.renderFile(`${templatesFolder}${template}/email.html.ejs`, {})
      .then(renderOutput => 
        new Promise((resolve, reject) => {
          fs.writeFile(`${templatesFolder}${template}/email.html`, renderOutput, (err) => {
            if(err) reject(err); else resolve();
          })
        })
      ) 
    )
  ).then(res => {
    console.log(`${res.length} templates compiled`);
  })
})